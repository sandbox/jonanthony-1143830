/**
 * Insert bullets and create glow effect
 */

(function($) {

	$(document)
			.ready(function() {
				 // $("#content").fadeIn();
					$("#leftbar li")
							.prepend(
									"<div class='imbul'></div>");
					$("#leftbar li").mousedown(
							function() {
								$("#content").fadeOut("fast");
								$("#leftbar li .imbul").stop()
										.animate( {
											opacity : 0
										}, 200);
							});
					$("#sitename a").mousedown(function() {
						$("#content").fadeOut("fast");
					});
					$("a").mouseout(function() {
						$("#content").show();
					});
				});

	$(function() {
		// OPACITY OF BUTTON SET TO 50%
		$("#leftbar li .imbul").css("opacity", "0");
		// ON MOUSE OVER
		$("#leftbar li").hover(function() {
			// SET OPACITY TO 1000%
				$('.imbul', this).stop().animate( {
					opacity : 1.0
				}, 500);
			},
			// ON MOUSE OUT
				function() {
					// SET OPACITY BACK TO 50%
					$('.imbul', this).stop().animate( {
						opacity : 0
					}, 3000);
				});
	});
})(jQuery);
